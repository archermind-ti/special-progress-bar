# SpecialProgressBar

## 简介
一个特殊的下载进度条组件

## 功能
进度条功能

## 演示

<img src="./demo/demo2.gif" width="20%" height="20%">

## 集成

方式一

1. 下载har包 https://gitee.com/archermind-ti/special-progress-bar/releases/1.0.2
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。

```
  dependencies {
     implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
     ……
  }
```

方式二

在project的build.gradle中添加mavenCentral()的引用

```
  repositories {   
 	...   
 	mavenCentral()   
	...           
  }
```
在entry的build.gradle中添加依赖

```
  dependencies { 
        ... 
        implementation 'com.gitee.archermind-ti:specialprogressbar_ohos:1.0.2' 
        ... 
  }
```



## 使用说明

1.  在xml中直接使用

```
    <com.wang.specialprogressbar.SpecialProgressBarView
        ohos:id="$+id:special_progress_bar"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:layout_alignment="center"/>
```

2.  在AbilitySlice中对其进行相关的设置

```
        mSpecialProgressBarView.setStartDrawable(ResourceTable.Media_ic_file_upload_white_36dp)
                .setTextColorNormal(Color.WHITE)
                .setEndSuccessBackgroundColor(new Color(0xFF66A269));
```
3.  设置动画介绍的监听，设置进度最大值，并发送定时消息更新进度

```
        mSpecialProgressBarView.setOnAnimationEndListener(() -> {
            mSpecialProgressBarView.setMax(100);
            mHandler.sendEvent(0);
        });
```

     MyEventHandler接受消息并处理设置更新进度

```
    class MyEventHandler extends EventHandler {
        private MyEventHandler(EventRunner runner) {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            int eventId = event.eventId;
            if (eventId == 0) {
                num++;
                if (num <= mSpecialProgressBarView.getMax()) {
                    mSpecialProgressBarView.setProgress(num);
                    mHandler.sendEvent(0, 50);
                }
            }
        }
    }
```



4.  设置进度的监听并回调显示

```
        mSpecialProgressBarView.setOnTextChangeListener(new SpecialProgressBarView.OnTextChangeListener() {
            @Override
            public String onProgressTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress) {
                return progress * 100 / max + "%";
            }

            @Override
            public String onErrorTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress) {
                return "error";
            }

            @Override
            public String onSuccessTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress) {
                return "done";
            }
        });
```

## 编译说明
- 1.将项目通过git clone 至本地
- 2.使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
- 3.点击Run运行即可（真机运行可能需要配置签名）


## 版本迭代

### 1.0.1
修复精度不跟随问题
### 1.0.2
修复进度气泡显示过小和 error / done 未显示在气泡内的问题

遗留问题：
- 1.鸿蒙没有提供绕z轴翻转api，点击出错或动画结束时3D翻转动效无法实现


## 版权和许可信息

Copyright 2019 zhangke

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
