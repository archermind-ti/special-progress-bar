# Version History
### Version v1.0.0
- 适配OpenHarmony api
- 遗留问题：动画api原因，按钮UI效果与原库有所差异

### 1.0.1
修复精度不跟随问题

### 1.0.2
修复进度气泡显示过小和 error / done 未显示在气泡内的问题