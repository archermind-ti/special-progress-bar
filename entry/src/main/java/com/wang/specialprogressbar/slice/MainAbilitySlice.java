package com.wang.specialprogressbar.slice;

import com.wang.specialprogressbar.ResourceTable;
import com.wang.specialprogressbar.SpecialProgressBarView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

public class MainAbilitySlice extends AbilitySlice {

    private int num = 0;
    private MyEventHandler mHandler;
    private SpecialProgressBarView mSpecialProgressBarView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mHandler = new MyEventHandler(EventRunner.current());

        mSpecialProgressBarView = (SpecialProgressBarView) findComponentById(ResourceTable.Id_special_progress_bar);
        mSpecialProgressBarView.setStartDrawable(ResourceTable.Media_ic_file_upload_white_36dp)
                .setTextColorNormal(Color.WHITE)
                .setEndSuccessBackgroundColor(new Color(0xFF66A269));

        Button btnStart = (Button) findComponentById(ResourceTable.Id_btn_start);
        btnStart.setClickedListener(component -> {
            num = 0;
            mSpecialProgressBarView.beginStarting();
        });

        Button btnError = (Button) findComponentById(ResourceTable.Id_btn_error);
        btnError.setClickedListener(component -> {
            mSpecialProgressBarView.setError();
        });

        mSpecialProgressBarView.setOnAnimationEndListener(() -> {
            mSpecialProgressBarView.setMax(100);
            mHandler.sendEvent(0);
        });

        mSpecialProgressBarView.setOnTextChangeListener(new SpecialProgressBarView.OnTextChangeListener() {
            @Override
            public String onProgressTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress) {
                return progress * 100 / max + "%";
            }

            @Override
            public String onErrorTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress) {
                return "error";
            }

            @Override
            public String onSuccessTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress) {
                return "done";
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    class MyEventHandler extends EventHandler {
        private MyEventHandler(EventRunner runner) {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            int eventId = event.eventId;
            if (eventId == 0) {
                num++;
                if (num <= mSpecialProgressBarView.getMax()) {
                    mSpecialProgressBarView.setProgress(num);
                    mHandler.sendEvent(0, 50);
                }
            }
        }
    }
}
