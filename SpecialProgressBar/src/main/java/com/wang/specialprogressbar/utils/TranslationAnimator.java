/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wang.specialprogressbar.utils;

import com.wang.specialprogressbar.SpecialProgressBarView;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

import java.util.ArrayList;
import java.util.List;


public class TranslationAnimator {

    private int mForm;
    private int mTo;
    private long mDuration;
    private AnimatorValue mAnimatorValue;
    private AnimationEndListener mAnimationEndListener;
    private AnimationValueUpdateListener mValueUpdateListener;
    private List<AnimatorValue> vas_List = new ArrayList<>();

    public AnimatorValue getAnimatorValue() {
        return mAnimatorValue;
    }

    public TranslationAnimator(Builder builder) {
        mForm = builder.form;
        mTo = builder.to;
        mDuration = builder.duration;
        mAnimationEndListener = builder.animationEndListener;
        mValueUpdateListener = builder.valueUpdateListener;
        mAnimatorValue = new AnimatorValue();
        mAnimatorValue.setDuration(mDuration);

        mAnimatorValue.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (mAnimationEndListener != null) {
                    mAnimationEndListener.onAnimationEnd(animator);
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        mAnimatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                int rate = mForm-mTo;
                mValueUpdateListener.onValueUpdate(rate, v);
            }
        });
    }

    public void start() {
        if (mAnimatorValue != null && !mAnimatorValue.isRunning()) {
            mAnimatorValue.start();
            vas_List.add(mAnimatorValue);
        }
    }

    public static class Builder {
        private int form;
        private int to;
        private long duration;
        private AnimationEndListener animationEndListener;
        private AnimationValueUpdateListener valueUpdateListener;

        public Builder setFrom(int form) {
            this.form = form;
            return this;
        }

        public Builder setTo(int to) {
            this.to = to;
            return this;
        }

        public Builder setDuration(long duration) {
            this.duration = duration;
            return this;
        }

        public Builder setAnimationEndListener(AnimationEndListener animationEndListener) {
            this.animationEndListener = animationEndListener;
            return this;
        }

        public Builder setValueUpdateListener(AnimationValueUpdateListener valueUpdateListener) {
            this.valueUpdateListener = valueUpdateListener;
            return this;
        }
    }

    public interface AnimationEndListener {
        void onAnimationEnd(Animator animator);
    }

    public interface AnimationValueUpdateListener {
        void onValueUpdate(int rate, float v);
    }
}
