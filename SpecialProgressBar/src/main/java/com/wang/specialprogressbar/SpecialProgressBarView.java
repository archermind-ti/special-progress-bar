package com.wang.specialprogressbar;


import com.wang.specialprogressbar.utils.ResUtil;
import com.wang.specialprogressbar.utils.TranslationAnimator;
import com.wang.specialprogressbar.utils.TypedAttrUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SpecialProgressBarView extends Component implements Component.EstimateSizeListener, Component.DrawTask {

    private Context mContext;

    private Paint mBgPaint, paint, mErrorPaint, mSuccessPaint;

    private Path p;
    private PixelMapElement downloadBitmap;

    private PixelMapElement loadingBitmap;

    private Paint mTextPaint;

    float[] POS = new float[2];
    float radius = 0;
    float scaleX = 0;
    float scaleY = 0;
    private String strText = "";

    float center_scaleX = 1;
    float center_scaleY = 1;
    private RectFloat moveBounds;
    //private Camera camera;
    private Matrix cameraMatrix = new Matrix();

    int rotateX = 0;
    int rotateY = 0;//成功时Y轴旋转的值

    private PathMeasure pm;//路径辅助工具
    int offsetY = 0;//摆幅偏移量
    float progressOffsetX = 0;//进度

    private static final int STATE_READY = 0;
    private static final int STATE_READY_CHANGEING = 1;
    private static final int STATE_READYING = 2;
    private static final int STATE_ERROR = 3;
    private static final int STATE_STARTING = 4;
    private static final int STATE_SUCCESS = 5;
    private static final int STATE_BACK = 6;
    private static final int STATE_BACK_HOME = 7;
    private static final int DONE = 8;

    private int state = STATE_READY;

    private int startX = 0, startY = 0, endX = 0, endY = 0;
    private List<AnimatorValue> vas_List = new ArrayList<>();

    private int max = 1;
    private int progress;

    private float pointStartX = -1f;
    float downX;//手指按下时的X坐标

    //动画结束监听  开始初始化进度设置
    private AnimationEndListener animationEndListener;

    private OnTextChangeListener ontextChangeListener;

    private Path pp = new Path();

    private int textSize;//字体大小
    private float progressBarHeight;//进度条宽度

    //成功字体颜色
    private static final int DEFAULT_TEXT_COLOR_SUCCESS = 0xFF66A269;
    private Color textColorSuccess = new Color(DEFAULT_TEXT_COLOR_SUCCESS);

    //失败字体颜色
    private static final int DEFAULT_TEXT_COLOR_ERROR = 0xFFBC5246;
    private Color textColorError = new Color(DEFAULT_TEXT_COLOR_ERROR);

    //默认字体颜色
    private static final int DEFAULT_TEXT_COLOR_NORMAL = 0xFF491C14;
    private Color textColorNormal = new Color(DEFAULT_TEXT_COLOR_NORMAL);

    //开始时图片
    private Element startDrawable;

    //进度条背景颜色
    private static final int DEFAULT_PROGRESS_BAR_BG_COLOR = 0xFF491C14;
    private Color progressBarBgColor = new Color(DEFAULT_PROGRESS_BAR_BG_COLOR);
    //进度条颜色
    private Color progressBarColor;
    //是否为第一次设置监听
    private boolean isFirstSetListener = false;
    //是否可以拖拽改变进度
    private boolean isCanDrag = true;
    //是否成功后返回原来效果
    private boolean isCanReBack = true;

    private Element endSuccessDrawable;
    private Color endSuccessBackgroundColor;
    //成功后是否可以再次点击
    private boolean isCanEndSuccessClickable = true;

    private boolean isTempCanEndSuccessClickable = true;

    //可点击区域
    private RectFloat rectClickRange;

    private Rect bounds;

    private EventHandler mEventHandler;


    public SpecialProgressBarView setCanEndSuccessClickable(boolean isCanEndSuccessClickable) {
        this.isTempCanEndSuccessClickable = isCanEndSuccessClickable;
        return this;
    }

    public SpecialProgressBarView setCanReBack(boolean isCanReBack) {
        this.isCanReBack = isCanReBack;
        return this;
    }

    /**
     * 设置当文字改变时监听
     *
     * @param onTextChangeListener
     */
    public void setOnTextChangeListener(OnTextChangeListener onTextChangeListener) {
        this.ontextChangeListener = onTextChangeListener;
    }

    /**
     * 是否可以拖拽
     *
     * @param isCanDrag
     */
    public SpecialProgressBarView setCanDragChangeProgress(boolean isCanDrag) {
        this.isCanDrag = isCanDrag;
        return this;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMax() {
        return max;
    }

    public void setProgress(int progress) {
        if (state != STATE_STARTING)
            return;
        this.progress = progress;
        if (progress == 0) {
            progressOffsetX = 0;
            return;
        }
        if (max == 0) {
            throw new RuntimeException("max不能为0!");
        }
        strText = progress * 100 / max + "%";
        progressOffsetX = (progress * (getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth()) * 1.0f / max);

        for (AnimatorValue va : vas_List) {
            if (va.isRunning()) {
                va.end();
            }
        }
        state = STATE_STARTING;

        invalidate();
    }

    /**
     * 进度出错
     */
    public void setError() {
        if (state == STATE_STARTING) {
            for (AnimatorValue va : vas_List) {
                if (va.isRunning()) {
                    va.end();
                }
            }
            state = STATE_ERROR;
            changeStateError();
        }
    }


    /**
     * 设置中间图片
     */
    public SpecialProgressBarView setStartDrawable(int resID) {
        this.startDrawable = ResUtil.getAnyElementByResId(mContext, resID);
        downloadBitmap = ResUtil.getPixelMapDrawable(mContext, resID);
        return this;
    }

    /**
     * 结束成功时
     */
    public SpecialProgressBarView setEndSuccessDrawable(int resID) {
        this.endSuccessDrawable = ResUtil.getAnyElementByResId(mContext, resID);
        return this;
    }

    public SpecialProgressBarView setEndSuccessBackgroundColor(Color color) {
        this.endSuccessBackgroundColor = color;
        return this;
    }

    /**
     * 设置当动画结束时监听
     *
     * @param mlistener
     */
    public void setOnAnimationEndListener(AnimationEndListener mlistener) {
        this.animationEndListener = mlistener;
        isFirstSetListener = true;
    }

    /**
     * 设置默认字体颜色
     */
    public SpecialProgressBarView setTextColorNormal(Color textColor) {
        this.textColorNormal = textColor;
        mTextPaint.setColor(textColorNormal);
        return this;
    }


    public SpecialProgressBarView(Context context) {
        this(context, null);
    }

    public SpecialProgressBarView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SpecialProgressBarView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.mContext = context;
        mEventHandler = new EventHandler(EventRunner.current());

        textSize = TypedAttrUtils.getDimensionPixelSize(attrSet, "textSize", 36);
        progressBarHeight = TypedAttrUtils.getDimension(attrSet, "textSize", dip2px(4));
        textColorSuccess = TypedAttrUtils.getColor(attrSet, "textColorSuccess", textColorSuccess);
        textColorError = TypedAttrUtils.getColor(attrSet, "textColorError", textColorError);
        textColorNormal = TypedAttrUtils.getColor(attrSet, "textColorNormal", textColorNormal);

        startDrawable = ResUtil.getAnyElementByResId(context, ResourceTable.Media_ic_get_app_white_36dp);
        endSuccessDrawable = ResUtil.getAnyElementByResId(context, ResourceTable.Media_ic_done_white_36dp);

        progressBarBgColor = TypedAttrUtils.getColor(attrSet, "progressBarBgColor", progressBarBgColor);
        progressBarColor = TypedAttrUtils.getColor(attrSet, "progressBarColor", Color.WHITE);

        isCanReBack = TypedAttrUtils.getBoolean(attrSet, "isCanReBack", true);
        isCanDrag = TypedAttrUtils.getBoolean(attrSet, "isCanDrag", true);
        endSuccessBackgroundColor = progressBarBgColor;

        initData(context);
        setEstimateSizeListener(this);
    }

    private void initData(Context context) {
        mBgPaint = new Paint();
        mBgPaint.setAntiAlias(true);
        mBgPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
        mBgPaint.setColor(progressBarBgColor);
        mBgPaint.setStyle(Paint.Style.STROKE_STYLE);
        mBgPaint.setStrokeWidth(progressBarHeight);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        paint.setColor(progressBarColor);
        paint.setStrokeWidth(progressBarHeight);
        paint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);

        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setStyle(Paint.Style.FILL_STYLE);
        mTextPaint.setColor(textColorNormal);
        mTextPaint.setFakeBoldText(true);//设置字体加粗
        mTextPaint.setTextSize(textSize);

        mErrorPaint = new Paint(mTextPaint);
        mErrorPaint.setColor(textColorError);
        mErrorPaint.setTextSize(textSize);

        mSuccessPaint = new Paint(mErrorPaint);
        mSuccessPaint.setAntiAlias(true);
        mSuccessPaint.setStyle(Paint.Style.FILL_STYLE);
        mSuccessPaint.setColor(textColorSuccess);
        mSuccessPaint.setTextSize(textSize);

        p = new Path();

        bounds = new Rect();
        downloadBitmap = ResUtil.getPixelMapDrawable(context, ResourceTable.Media_ic_get_app_white_36dp);

        loadingBitmap = ResUtil.getPixelMapDrawable(context, ResourceTable.Media_ic_chat_bubble_white_36dp);

        //camera = new Camera();

        addDrawTask(this);
    }

    /**
     * 初始化状态
     */
    private void initStateData() {
        center_scaleX = 1;
        center_scaleY = 1;
        rotateX = 0;
        rotateY = 0;
        radius = 0;//半径
        scaleX = 0;
        scaleY = 0;
        strText = "";
        startX = getWidth() / 2;
        startY = getHeight() / 2;
        endX = getWidth() / 2;
        endY = getHeight() / 2;
        state = STATE_READY;
        progressOffsetX = 0;
        offsetY = 0;
        //progress = 0;
        pointStartX = -1f;
        downX = 0;
        moveBounds = null;
        paint.setColor(progressBarColor);
        downloadBitmap = ResUtil.getPixelMapDrawable(getContext(), ResourceTable.Media_ic_get_app_white_36dp);
        vas_List.clear();
        mEventHandler.removeAllEvent();
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int width = EstimateSpec.getSize(widthMeasureSpec);
        int height = EstimateSpec.getSize(heightMeasureSpec);

        // 自定义布局高度
        int estimateHeight = loadingBitmap.getHeight() * 3;

        /*setEstimatedSize(
                Component.EstimateSpec.getChildSizeWithMode(width, width, EstimateSpec.NOT_EXCEED),
                Component.EstimateSpec.getChildSizeWithMode(height, height, Component.EstimateSpec.NOT_EXCEED));*/

        setEstimatedSize(
                EstimateSpec.getChildSizeWithMode(width, width, EstimateSpec.PRECISE),
                EstimateSpec.getChildSizeWithMode(estimateHeight, estimateHeight, EstimateSpec.NOT_EXCEED));

        startX = getWidth() / 2;
        startY = getHeight() / 2;
        endX = getWidth() / 2;
        endY = getHeight() / 2;
        radius = (Math.min(getWidth(), getHeight()) - mBgPaint.getStrokeWidth() ) / 2;
        rectClickRange = new RectFloat(width / 2 - radius, height / 2 - radius,
                width / 2 + radius, height / 2 + radius);

        if (progress == 0) {
            progressOffsetX = 0;
        } else {
            progressOffsetX = (progress * (width - mBgPaint.getStrokeWidth() - downloadBitmap.getWidth()) * 1.0f / max);
        }
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        switch (state) {
            case STATE_BACK_HOME:
            case STATE_READY:
                p.reset();
                mBgPaint.setStyle(Paint.Style.FILL_STYLE);

                p.addCircle(component.getWidth() / 2, component.getHeight() / 2, radius * 0.8f, Path.Direction.COUNTER_CLOCK_WISE);
                canvas.drawPath(p, mBgPaint);

                PixelMapHolder startHolder = new PixelMapHolder(downloadBitmap.getPixelMap());
                canvas.drawPixelMapHolder(startHolder, component.getWidth() / 2 - downloadBitmap.getWidth() / 2,
                        component.getHeight() / 2 - downloadBitmap.getHeight() / 2, mBgPaint);
                break;
            case STATE_READY_CHANGEING:
                p.reset();
                p.moveTo(startX, startY);
                p.lineTo(endX, endY);
                canvas.drawPath(p, mBgPaint);
                break;
            case STATE_READYING:
                p.reset();
                p.moveTo(mBgPaint.getStrokeWidth(), component.getHeight() / 2);//-offsetY*0.6f
                if (offsetY == 0) {
                    p.quadTo(mBgPaint.getStrokeWidth(), component.getHeight() / 2,
                            component.getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth(), component.getHeight() / 2);
                } else {
                    p.quadTo(component.getWidth() / 2, component.getHeight() / 2 - offsetY,
                            component.getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth(), component.getHeight() / 2);//-offsetY*0.6f
                }
                canvas.drawPath(p, mBgPaint);

                PixelMapHolder holder = new PixelMapHolder(loadingBitmap.getPixelMap());
                canvas.drawPixelMapHolder(holder, progressOffsetX, component.getHeight() / 2 - mBgPaint.getStrokeWidth() / 2 - downloadBitmap.getHeight(), mBgPaint);
                break;
            case STATE_BACK:
                /*canvas.drawPixelMapHolder(new PixelMapHolder(loadingBitmap.getPixelMap()), POS[0],
                        POS[1] - mBgPaint.getStrokeWidth() / 2 - downloadBitmap.getHeight() * scaleY, mBgPaint);*/

                p.reset();
                p.moveTo(startX, getHeight() / 2);//-offsetY*0.6f
                p.quadTo(startX, getHeight() / 2, endX, getHeight() / 2);//-offsetY*0.6f

                canvas.drawPath(p, paint);
                break;
            case STATE_SUCCESS:
                /*canvas.drawPixelMapHolder(new PixelMapHolder(loadingBitmap.getPixelMap()),
                        endX - loadingBitmap.getWidth() * 2, getHeight() - loadingBitmap.getHeight()*2, mBgPaint);*/

                if (rotateY >= 360) {
                    strText = "done";
                    if (ontextChangeListener != null && ontextChangeListener.onSuccessTextChange(this, max, progress) != null) {
                        strText = ontextChangeListener.onSuccessTextChange(this, max, progress);
                    }
                    mSuccessPaint.getTextBounds(strText);
                    /*canvas.drawText(mSuccessPaint, strText, POS[0] + loadingBitmap.getWidth() / 2 - bounds.right / 2,
                            POS[1] - loadingBitmap.getHeight() / 2 + bounds.bottom);*/
                    canvas.drawText(mSuccessPaint, strText, endX - loadingBitmap.getWidth() * 2,
                            getHeight() - loadingBitmap.getHeight() - 10);
                }
                p.reset();
                p.moveTo(mBgPaint.getStrokeWidth(), getHeight() / 2);//-offsetY*0.6f
                p.quadTo(mBgPaint.getStrokeWidth(), getHeight() / 2, getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth(), getHeight() / 2);//-offsetY*0.6f

                canvas.drawPath(p, paint);
                break;
            case STATE_ERROR:
            case STATE_STARTING:
                p.reset();
                p.moveTo(mBgPaint.getStrokeWidth(), component.getHeight() / 2);
                if (progressOffsetX > radius) {
                    if (progressOffsetX >= (component.getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth())) {
                        p.quadTo(component.getWidth() / 2, component.getHeight() / 2 - offsetY,
                                component.getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth(), component.getHeight() / 2);//-offsetY*0.6f
                    } else {
                        p.quadTo(progressOffsetX, component.getHeight(),
                                component.getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth(), getHeight() / 2);
                    }
                } else {
                    p.quadTo(component.getWidth() / 2, component.getHeight() / 2 - offsetY,
                            component.getWidth() - mBgPaint.getStrokeWidth() - loadingBitmap.getWidth(), component.getHeight() / 2);//-offsetY*0.6f
                }
                canvas.drawPath(p, mBgPaint);

                pm = new PathMeasure(p, false);
                pp.reset();
                pp.rLineTo(0, 0);
                if (progressOffsetX >= pm.getLength()) {//(getWidth()-mBgPaint.getStrokeWidth() -loadingBitmap.getWidth())
                    pm.getSegment(0, pm.getLength(), pp, true);
                    //这里数据存入POS一直失败
                    pm.getPosTan(pm.getLength(), POS, null);
                } else {
                    pm.getSegment(0, progressOffsetX, pp, true);
                    //这里数据存入POS一直失败
                    pm.getPosTan(progressOffsetX, POS, null);
                }
                canvas.drawPath(pp, paint);

                moveBounds = new RectFloat(POS[0], POS[1] - loadingBitmap.getHeight(), POS[0] + loadingBitmap.getWidth(), POS[1]);

                if (state == STATE_ERROR) {
                    canvas.save();
                    canvas.translate(0,0);
                   /* canvas.scale(1,-1,100,100);*/
                    canvas.drawPixelMapHolder(new PixelMapHolder(loadingBitmap.getPixelMap()), POS[0] + loadingBitmap.getWidth() / 2-50,
                            POS[1] + loadingBitmap.getHeight() / 2-150, mBgPaint);
                    canvas.restore();
                } else {
                    if (state != STATE_SUCCESS) {
                        canvas.drawPixelMapHolder(new PixelMapHolder(loadingBitmap.getPixelMap()), POS[0],
                                POS[1] - mBgPaint.getStrokeWidth() / 2 - downloadBitmap.getHeight(), mBgPaint);
                    }
                }
                bounds.set(0, 0, 0, 0);
                if (progressOffsetX >= pm.getLength()) {
                    progressOffsetX = pm.getLength();
                }
                strText = (int) (progressOffsetX * 100 / pm.getLength()) + "%";
                if (ontextChangeListener != null && ontextChangeListener.onProgressTextChange(this, max, progress) != null) {
                    strText = ontextChangeListener.onProgressTextChange(this, max, progress);
                }
                mTextPaint.getTextBounds(strText);
                if (state == STATE_ERROR) {
                    moveBounds = new RectFloat(POS[0], POS[1], POS[0] + loadingBitmap.getWidth(), POS[1] + loadingBitmap.getHeight());
                    if (rotateX <= -100) {
                        strText = "fail";
                        if (ontextChangeListener != null && ontextChangeListener.onErrorTextChange(this, max, progress) != null) {
                            strText = ontextChangeListener.onErrorTextChange(this, max, progress);
                        }
                        mErrorPaint.setColor(Color.RED);
                        mErrorPaint.getTextBounds(strText);
                        mErrorPaint.setTextSize(35);
                        canvas.drawText(mErrorPaint, strText, POS[0] + mBgPaint.getStrokeWidth()+5, POS[1] + loadingBitmap.getHeight() * 5 / 6-125);
                        /*canvas.drawText(mErrorPaint, strText, endX - loadingBitmap.getWidth() * 2,
                                getHeight() - loadingBitmap.getHeight() - 10);*/
                    }
                } else {
                    if (state != STATE_SUCCESS) {
                        mTextPaint.setColor(Color.BLACK);
                        mTextPaint.setTextSize(40);
                        canvas.drawText(mTextPaint, strText, POS[0] + loadingBitmap.getWidth() / 2 - bounds.right / 2-40,
                                POS[1] - loadingBitmap.getHeight() / 2 - bounds.bottom / 2);

                      /*  canvas.drawText(mTextPaint, strText, endX - loadingBitmap.getWidth() * 2,
                                getHeight() - loadingBitmap.getHeight() - 10);*/
                    }
                }
                if (progressOffsetX == pm.getLength() && state != STATE_SUCCESS) {
                    changeStateSuccess();
                }
                break;
        }
    }


    /**
     * 开始
     */
    public void beginStarting() {
        initStateData();
        if (state == STATE_READY) {
            TranslationAnimator animator = new TranslationAnimator(new TranslationAnimator.Builder()
                    .setDuration(1000)
                    .setFrom((int) (Math.min(getWidth(), getHeight()) - mBgPaint.getStrokeWidth() * 2) / 2)
                    .setTo((int) mBgPaint.getStrokeWidth())
                    .setValueUpdateListener((rate, v) -> {
                        radius = (1 - v) * rate;
                        center_scaleX = v;
                        center_scaleY = v;
                        invalidate();
                    })
                    .setAnimationEndListener(animator1 -> {
                        state = STATE_READY_CHANGEING;//准备阶段
                        mBgPaint.setStyle(Paint.Style.STROKE_STYLE);
                        mBgPaint.setColor(Color.BLACK);
                        changeStateReadyChanging();
                    }));
            animator.start();
            vas_List.add(animator.getAnimatorValue());
        }
    }

    private void changeStateReadyChanging() {
        TranslationAnimator animator = new TranslationAnimator(new TranslationAnimator.Builder()
                .setDuration(800)
                .setFrom(getWidth() / 2 - (int) mBgPaint.getStrokeWidth())
                .setTo((int) mBgPaint.getStrokeWidth())
                .setValueUpdateListener((rate, v) -> {
                    startX = (int) (getWidth() / 2 - v * rate);
                    endX = (int) (getWidth() / 2 + v * rate);
                    invalidate();
                })
                .setAnimationEndListener(animator1 -> {
                    changeStateStart();
                }));
        animator.start();
        vas_List.add(animator.getAnimatorValue());
    }

    private void changeStateStart() {
        state = STATE_READYING;
        offsetY = 0;
        invalidate();

        TranslationAnimator animator = new TranslationAnimator(new TranslationAnimator.Builder()
                .setDuration(800)
                .setFrom(getHeight() / 2)
                .setTo(0)
                .setValueUpdateListener((rate, v) -> {
                    offsetY = (int) ((1 - v) * rate);
                    scaleX = v;
                    scaleY = v;
                    if (v >= 1.0f) {
                        state = STATE_STARTING;
                    }
                    invalidate();
                })
                .setAnimationEndListener(animator1 -> {
                    if (animationEndListener != null) {
                        mEventHandler.postTask(new Runnable() {
                            @Override
                            public void run() {
                                animationEndListener.onAnimationEnd();
                                strText = progress * 100 / max + "%";
                                isFirstSetListener = false;
                            }
                        }, 200);
                    } else {
                        isFirstSetListener = false;
                    }
                }));
        animator.start();
        vas_List.add(animator.getAnimatorValue());
    }

    private void changeStateSuccess() {
        if (state != STATE_SUCCESS) {
            this.progress = max;
            state = STATE_SUCCESS;
            TranslationAnimator animator = new TranslationAnimator(new TranslationAnimator.Builder()
                    .setDuration(800)
                    .setFrom(360)
                    .setTo(0)
                    .setValueUpdateListener((rate, v) -> {
                        rotateY = (int) (v * rate);
                        invalidate();
                    })
                    .setAnimationEndListener(animator1 -> {
                        if (isCanReBack) {
                            changeStateBack();
                        }
                    }));
            animator.start();
            vas_List.add(animator.getAnimatorValue());
        }
    }

    private void changeStateBack() {
        state = STATE_BACK;
        TranslationAnimator animator = new TranslationAnimator(new TranslationAnimator.Builder()
                .setDuration(800)
                .setFrom(getWidth() / 2 - (int) mBgPaint.getStrokeWidth())
                .setTo((int) mBgPaint.getStrokeWidth())
                .setValueUpdateListener((rate, v) -> {
                    int value = (int) ((1 - v) * rate);
                    startX = getWidth() / 2 + value;
                    endX = getWidth() / 2 - value;
                    POS[0] = startX;
                    scaleX = 1 - v;
                    scaleY = 1 - v;
                    invalidate();
                })
                .setAnimationEndListener(animator1 -> {
                    changeStateBackHome();
                }));
        animator.start();
        vas_List.add(animator.getAnimatorValue());
    }

    private void changeStateBackHome() {
        state = STATE_BACK_HOME;

        downloadBitmap = (PixelMapElement) endSuccessDrawable;

        mBgPaint.setColor(endSuccessBackgroundColor);
        TranslationAnimator animator = new TranslationAnimator(new TranslationAnimator.Builder()
                .setDuration(800)
                .setFrom((int) (Math.min(getWidth(), getHeight()) - mBgPaint.getStrokeWidth() * 2) / 2)
                .setTo((int) mBgPaint.getStrokeWidth())
                .setValueUpdateListener((rate, v) -> {
                    radius = v * rate;
                    center_scaleX = v;
                    center_scaleY = v;
                    invalidate();
                })
                .setAnimationEndListener(animator1 -> {
                    isCanEndSuccessClickable = isTempCanEndSuccessClickable;
                    mEventHandler.postTask(new Runnable() {
                        @Override
                        public void run() {
                            state = DONE;
                        }
                    }, 500);
                }));
        animator.start();
        vas_List.add(animator.getAnimatorValue());
    }

    public void changeStateError() {
        TranslationAnimator animator = new TranslationAnimator(new TranslationAnimator.Builder()
                .setDuration(300)
                .setFrom(0)
                .setTo(180)
                .setValueUpdateListener((rate, v) -> {
                    rotateX = (int) (v * (-180));
                    invalidate();
                }));
        animator.start();
        vas_List.add(animator.getAnimatorValue());
    }

    public interface OnTextChangeListener {
        //进度变化时调用
        String onProgressTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress);

        //失败
        String onErrorTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress);

        //成功
        String onSuccessTextChange(SpecialProgressBarView specialProgressBarView, int max, int progress);
    }

    /**
     * 动画结束监听
     */
    public interface AnimationEndListener {
        void onAnimationEnd();
    }

    private int dip2px(int dp) {
        Optional<Display> defaultDisplay = DisplayManager.getInstance().getDefaultDisplay(getContext());
        DisplayAttributes attributes = defaultDisplay.get().getAttributes();
        float density = attributes.scalDensity;
        return (int) (dp * density + 0.5f);
    }
}
